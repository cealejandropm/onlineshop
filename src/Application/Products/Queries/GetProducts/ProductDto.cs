﻿using onlineshop.Application.Common.Mappings;
using onlineshop.Domain.Entities;
using onlineshop.Domain.ValueObjects;
using onlineshop.Domain.Enums;
using System.Collections.Generic;
using AutoMapper;

namespace onlineshop.Application.Products.Queries.GetProducts
{
    public class ProductDto : IMapFrom<Product>
    {
        public ProductDto()
        {
        }

        public int Id { get; set; }
        public int Size { get; set; }
        public int Color { get; set; }
        public Price Price { get; set; }
        public string Description { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Product, ProductDto>()
                .ForMember(d => d.Size, opt => opt.MapFrom(s => (int)s.Size))
                .ForMember(d => d.Color, opt => opt.MapFrom(s => (int)s.Color));
        }
    }
}
