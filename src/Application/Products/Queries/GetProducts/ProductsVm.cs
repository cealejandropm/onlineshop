﻿using onlineshop.Domain.Enums;
using onlineshop.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace onlineshop.Application.Products.Queries.GetProducts
{
    public class ProductsVm
    {
        public IList<ProductDto> Lists { get; set; }
    }
}
