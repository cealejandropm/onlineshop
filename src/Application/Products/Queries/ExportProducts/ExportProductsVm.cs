﻿namespace onlineshop.Application.Products.Queries.ExportProducts
{
    public class ExportProductsVm
    {
        public string FileName { get; set; }

        public string ContentType { get; set; }

        public byte[] Content { get; set; }
    }
}