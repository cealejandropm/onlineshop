﻿using AutoMapper;
using onlineshop.Application.Common.Mappings;
using onlineshop.Domain.Entities;
using onlineshop.Domain.ValueObjects;
using System.Drawing;

namespace onlineshop.Application.Products.Queries.ExportProducts
{
    public class ProductRecord : IMapFrom<Product>
    {
        public int Size { get; set; }
        public int Color { get; set; }
        public Price Price { get; set; }
        public string Description { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Product, ProductRecord>()
                .ForMember(d => d.Size, opt => opt.MapFrom(s => (int)s.Size))
                .ForMember(d => d.Color, opt => opt.MapFrom(s => (int)s.Color));
        }
    }
}
