﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using onlineshop.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace onlineshop.Application.Products.Queries.ExportProducts
{
    public class ExportProductsQuery : IRequest<ExportProductsVm>
    {
        public int Id { get; set; }

        public class ExportTodosQueryHandler : IRequestHandler<ExportProductsQuery, ExportProductsVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly ICsvFileBuilder<ProductRecord> _fileBuilder;

            public ExportTodosQueryHandler(IApplicationDbContext context, IMapper mapper, ICsvFileBuilder<ProductRecord> fileBuilder)
            {
                _context = context;
                _mapper = mapper;
                _fileBuilder = fileBuilder;
            }

            public async Task<ExportProductsVm> Handle(ExportProductsQuery request, CancellationToken cancellationToken)
            {
                var vm = new ExportProductsVm();

                var records = await _context.Products
                        .Where(t => t.Id == request.Id)
                        .ProjectTo<ProductRecord>(_mapper.ConfigurationProvider)
                        .ToListAsync(cancellationToken);

                vm.Content = _fileBuilder.BuildFile(records);
                vm.ContentType = "text/csv";
                vm.FileName = "Products.csv";

                return await Task.FromResult(vm);
            }
        }
    }
}
