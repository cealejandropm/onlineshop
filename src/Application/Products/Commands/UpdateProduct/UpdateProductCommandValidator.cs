﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using onlineshop.Application.Common.Interfaces;
using onlineshop.Domain.ValueObjects;
using System.Threading;
using System.Threading.Tasks;

namespace onlineshop.Application.Products.Commands.UpdateProduct
{
    public class UpdateProductCommandValidator : AbstractValidator<UpdateProductCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateProductCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Id).NotEmpty().WithMessage("Id is required.");
        }

    }
}
