﻿using onlineshop.Application.Common.Exceptions;
using onlineshop.Application.Common.Interfaces;
using onlineshop.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using onlineshop.Domain.ValueObjects;
using onlineshop.Domain.Enums;

namespace onlineshop.Application.Products.Commands.UpdateProduct
{
    public partial class UpdateProductCommand : IRequest
    {
        public int Id { get; set; }

        public Size Size { get; set; }
        public Color Color { get; set; }
        public string PriceStr { get; set; }
        public string Description { get; set; }

        public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateProductCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.Products.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(Product), request.Id);
                }
                if (request.Size > 0)
                    entity.Size = request.Size;
                if (request.Color > 0)
                    entity.Color = request.Color;
                if (!string.IsNullOrEmpty(request.PriceStr))
                    entity.Price = Price.For(request.PriceStr);
                entity.Description = request.Description;

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
