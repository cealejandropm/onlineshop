﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using onlineshop.Application.Common.Interfaces;
using onlineshop.Domain.ValueObjects;
using System.Threading;
using System.Threading.Tasks;

namespace onlineshop.Application.Products.Commands.CreateProduct
{
    public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateProductCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.PriceStr)
                .NotEmpty().WithMessage("Price is required.")
                .MustAsync(BePositivePrice).WithMessage("The specified price must be positive.");
        }

        public async Task<bool> BePositivePrice(string price, CancellationToken cancellationToken)
        {
            try
            {
                Price.For(price);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            };
        }
    }
}
