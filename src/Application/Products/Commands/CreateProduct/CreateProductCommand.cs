﻿using onlineshop.Application.Common.Interfaces;
using onlineshop.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using onlineshop.Domain.ValueObjects;
using onlineshop.Domain.Enums;

namespace onlineshop.Application.Products.Commands.CreateProduct
{
    public class CreateProductCommand : IRequest<int>
    {
        public int Size { get; set; }
        public int Color { get; set; }
        public string PriceStr { get; set; }
        public string Description { get; set; }

        public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateProductCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateProductCommand request, CancellationToken cancellationToken)
            {
                var entity = new Product();

                entity.Size = (Size)request.Size;
                entity.Color = (Color)request.Size;
                entity.Description = request.Description;
                entity.Price = Price.For(request.PriceStr);
                _context.Products.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return entity.Id;
            }
        }
    }
}
