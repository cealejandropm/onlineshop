﻿using System.Collections.Generic;

namespace onlineshop.Application.Common.Interfaces
{
    public interface ICsvFileBuilder<T>
    {
        byte[] BuildFile(IEnumerable<T> records);
    }
}
