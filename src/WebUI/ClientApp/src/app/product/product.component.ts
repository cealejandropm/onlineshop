import { Component, TemplateRef } from '@angular/core';
import { ProductsClient, CreateProductCommand, UpdateProductCommand, ProductDto, ProductsVm, Price, Color, Size } from '../onlineshop-api';
import { faPlus, faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-product-component',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {

  debug: boolean;
  productsVm: ProductsVm = new ProductsVm();

  public Color = Object.keys(Color).filter(
    (type) => isNaN(<any>type) && type !== 'values'
  );
  public Size = Object.keys(Size).filter(
    (type) => isNaN(<any>type) && type !== 'values'
  );
  selectedProduct: ProductDto;
  colorType = Color;
  sizeType = Size;

  newProductEditor: any = {};
  productOptionsEditor: any = {};
  productDetailsEditor: any = {};

  newProductModalRef: BsModalRef;
  productOptionsModalRef: BsModalRef;
  deleteProductModalRef: BsModalRef;
  productDetailsModalRef: BsModalRef;

  faPlus = faPlus;
  faEllipsisH = faEllipsisH;

  constructor(private client: ProductsClient, private modalService: BsModalService) {
    client.get().subscribe(result => {
      this.productsVm = result;
    }, error => console.error(error));
  }

  showNewProductModal(template: TemplateRef<any>): void {
    this.newProductModalRef = this.modalService.show(template);
    setTimeout(() => document.getElementById("id").focus(), 250);
  }

  newProductCancelled(): void {
    this.newProductModalRef.hide();
    this.newProductEditor = {};
  }

  addProduct(): void {
    let product = ProductDto.fromJS({
      id: 0,
      size: this.newProductEditor.size,
      color: this.newProductEditor.color,
      price: new Price({
        ammount: this.newProductEditor.ammount,
        currency: this.newProductEditor.currency
      }),
      description: this.newProductEditor.description,
    });

    this.client.create(<CreateProductCommand>{
      size: this.newProductEditor.size,
      color: this.newProductEditor.color,
      priceStr: `${this.newProductEditor.currency} ${this.newProductEditor.ammount}`,
      description: this.newProductEditor.description
    }).subscribe(
      result => {
        product.id = result;
        this.productsVm.lists.push(product);
        this.selectedProduct = product;
        this.newProductModalRef.hide();
        this.newProductEditor = {};
      },
      error => {
        let errors = JSON.parse(error.response);

        if (errors && errors.Title) {
          this.newProductEditor.error = errors.Title[0];
        }

        setTimeout(() => document.getElementById("title").focus(), 250);
      }
    );
  }

  showProductDetailsModal(template: TemplateRef<any>, product: ProductDto): void {
    this.selectedProduct = product;
    this.productDetailsEditor = {
      ...this.selectedProduct
    };

    this.productDetailsModalRef = this.modalService.show(template);
  }

  updateProductDetails(): void {
    this.client.update(this.selectedProduct.id, UpdateProductCommand.fromJS(this.productDetailsEditor))
      .subscribe(
        () => {
          if (this.selectedProduct.id != this.productDetailsEditor.id) {
            let listIndex = this.productsVm.lists.findIndex(l => l.id == this.productDetailsEditor.id);
            this.selectedProduct.id = this.productDetailsEditor.id;
            this.productsVm.lists[listIndex] = this.selectedProduct;
          }
          this.selectedProduct.size = this.productDetailsEditor.size;
          this.selectedProduct.color = this.productDetailsEditor.color;
          this.selectedProduct.description = this.productDetailsEditor.description;
          this.selectedProduct.price = this.productDetailsEditor.price;
          this.productDetailsModalRef.hide();
          this.productDetailsEditor = {};
        },
        error => console.error(error)
      );
  }

  showProductOptionsModal(template: TemplateRef<any>) {
    this.productOptionsEditor = {
      id: this.selectedProduct.id,
      size: this.selectedProduct.size,
      color: this.selectedProduct.color,
      price: this.selectedProduct.price,
      description: this.selectedProduct.description,
    };

    this.productOptionsModalRef = this.modalService.show(template);
  }

  updateProductOptions() {
    this.client.update(this.selectedProduct.id, UpdateProductCommand.fromJS(this.productOptionsEditor))
      .subscribe(
        () => {
          this.selectedProduct.size = this.productOptionsEditor.size,
            this.selectedProduct.color = this.productOptionsEditor.color,
            this.selectedProduct.price = this.productOptionsEditor.price,
            this.selectedProduct.description = this.productOptionsEditor.description,
            this.productOptionsModalRef.hide();
          this.productOptionsEditor = {};
        },
        error => console.error(error)
      );
  }

  confirmDeleteProduct(template: TemplateRef<any>, product: ProductDto) {
    this.selectedProduct = product;
    this.deleteProductModalRef = this.modalService.show(template);
  }

  deleteProductConfirmed(): void {
    this.client.delete(this.selectedProduct.id).subscribe(
      () => {
        this.deleteProductModalRef.hide();
        this.productsVm.lists = this.productsVm.lists.filter(t => t.id != this.selectedProduct.id)
        this.selectedProduct = this.productsVm.lists.length ? this.productsVm.lists[0] : null;
      },
      error => console.error(error)
    );
  }

}
