﻿namespace onlineshop.Domain.Enums
{
    public enum Color
    {
        Black, White, Gray, Brown, Beige, Red, Orange, Green, Blue, Yellow, Purple
    }
}
