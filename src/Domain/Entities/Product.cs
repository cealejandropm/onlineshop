﻿using onlineshop.Domain.Common;
using onlineshop.Domain.Enums;
using onlineshop.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace onlineshop.Domain.Entities
{
    public class Product : AuditableEntity
    {
        public int Id { get; set; }
        public Size Size { get; set; }
        public Color Color { get; set; }
        public Price Price { get; set; }
        public string Description { get; set; }
    }
}
