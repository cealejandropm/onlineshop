﻿using System;

namespace onlineshop.Domain.Exceptions
{
    public class PriceInvalidException : Exception
    {
        public PriceInvalidException(double Ammount)
            : base($"Price {Ammount} is invalid. The amount has to be greater than 0")
        {
        }

        public PriceInvalidException(string price, Exception ex)
            : base($"Price \"{price}\" is invalid.", ex)
        {
        }
    }
}
