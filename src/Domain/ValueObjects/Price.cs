﻿using onlineshop.Domain.Common;
using onlineshop.Domain.Exceptions;
using System;
using System.Collections.Generic;

namespace onlineshop.Domain.ValueObjects
{
    public class Price : ValueObject
    {
        public double Ammount { get; private set; }
        public string Currency { get; private set; }

        private Price() { }

        public static Price For(string priceString)
        {
            var price = new Price();

            try
            {
                var index = priceString.IndexOf(" ", StringComparison.Ordinal);
                price.Currency = priceString.Substring(0, index);
                price.Ammount = double.Parse(priceString.Substring(index + 1));
                if (price.Ammount < 0)
                {
                    throw new PriceInvalidException(price.Ammount);
                }
            }
            catch (Exception ex)
            {
                throw new PriceInvalidException(priceString, ex);
            }

            return price;
        }

        public static implicit operator string(Price price)
        {
            return price.ToString();
        }

        public static explicit operator Price(string priceString)
        {
            return For(priceString);
        }

        public override string ToString()
        {
            return $"{Currency} {Ammount}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Currency;
            yield return Ammount;
        }
    }
}