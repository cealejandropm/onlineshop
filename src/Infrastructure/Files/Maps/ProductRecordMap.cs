﻿using CsvHelper.Configuration;
using onlineshop.Application.Products.Queries.ExportProducts;

namespace onlineshop.Infrastructure.Files.Maps
{
    public class ProductRecordMap : ClassMap<ProductRecord>
    {
        public ProductRecordMap()
        {
            AutoMap();
        }
    }
}
