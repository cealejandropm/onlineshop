﻿using onlineshop.Application.Common.Interfaces;
using onlineshop.Infrastructure.Files.Maps;
using CsvHelper;
using System.Collections.Generic;
using System.IO;

namespace onlineshop.Infrastructure.Files
{
    public class CsvFileBuilder<T> : ICsvFileBuilder<T>
    {
        public byte[] BuildFile(IEnumerable<T> records)
        {
            using var memoryStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(memoryStream))
            {
                using var csvWriter = new CsvWriter(streamWriter);

                csvWriter.Configuration.RegisterClassMap<ProductRecordMap>();
                csvWriter.WriteRecords(records);
            }

            return memoryStream.ToArray();
        }
    }
}
