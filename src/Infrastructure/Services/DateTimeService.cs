﻿using onlineshop.Application.Common.Interfaces;
using System;

namespace onlineshop.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
