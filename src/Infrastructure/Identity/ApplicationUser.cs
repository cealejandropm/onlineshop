﻿using Microsoft.AspNetCore.Identity;

namespace onlineshop.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
