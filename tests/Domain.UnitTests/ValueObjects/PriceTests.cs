﻿using onlineshop.Domain.Exceptions;
using onlineshop.Domain.ValueObjects;
using Shouldly;
using Xunit;

namespace onlineshop.Domain.UnitTests.ValueObjects
{
    public class PriceTests
    {
        [Fact]
        public void ShouldHaveCorrectPrice()
        {
            const double ammount = 25;
            const string currency = "USD";

            var price = Price.For($"{currency} {ammount}");

            price.Ammount.Equals(ammount);
            price.Currency.ShouldBe(currency);
        }

        [Fact]
        public void ToStringReturnsCorrectFormat()
        {
            const string priceString = "USD 25";

            var price = Price.For(priceString);

            var result = price.ToString();

            result.ShouldBe(priceString);
        }

        [Fact]
        public void ImplicitConversionToStringResultsInCorrectString()
        {
            const string priceString = "USD 25";

            var price = Price.For(priceString);

            string result = price;

            result.ShouldBe(priceString);
        }

        [Fact]
        public void ExplicitConversionFromStringSetsCurrencyAndAmmount()
        {
            const string priceString = "USD 25";

            var price = (Price)priceString;

            price.Currency.ShouldBe("USD");
            price.Ammount.ShouldBe(25);
        }

        [Fact]
        public void ShouldThrowPriceInvalidExceptionForInvalidPrice()
        {
            Assert.Throws<PriceInvalidException>(() => Price.For("-1"));
        }
    }
}
