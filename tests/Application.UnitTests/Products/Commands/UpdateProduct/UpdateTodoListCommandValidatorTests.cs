﻿using onlineshop.Application.Products.Commands.UpdateProduct;
using onlineshop.Application.UnitTests.Common;
using onlineshop.Domain.Entities;
using onlineshop.Domain.ValueObjects;
using Shouldly;
using Xunit;

namespace onlineshop.Application.UnitTests.Products.Commands.UpdateProduct
{
    public class UpdateProductCommandValidatorTests : CommandTestBase
    {
        [Fact]
        public void IsValid_ShouldBeTrue_WhenProductPriceIsPositive()
        {
            var command = new UpdateProductCommand
            {
                Id = 1,
                PriceStr = "USD 34"
            };

            var validator = new UpdateProductCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(true);
        }

        [Fact]
        public void IsValid_ShouldBeFalse_WhenProductIdIsNull()
        {
            var command = new UpdateProductCommand
            {
                Description = "Id = Null"
            };

            var validator = new UpdateProductCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(false);
        }
    }
}
