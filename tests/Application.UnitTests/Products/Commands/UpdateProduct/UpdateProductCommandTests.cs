﻿using onlineshop.Application.Common.Exceptions;
using onlineshop.Application.Products.Commands.UpdateProduct;
using onlineshop.Application.UnitTests.Common;
using onlineshop.Domain.Enums;
using onlineshop.Domain.ValueObjects;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace onlineshop.Application.UnitTests.Products.Commands.UpdateProduct
{
    public class UpdateProductCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_GivenValidId_ShouldUpdatePersistedProduct()
        {
            var command = new UpdateProductCommand
            {
                Id = 1,
                Color = Color.White,
                Size= Size.M,
                Description= "White Tshirt",
                PriceStr = "USD 24"
            };

            var handler = new UpdateProductCommand.UpdateProductCommandHandler(Context);

            await handler.Handle(command, CancellationToken.None);

            var entity = Context.Products.Find(command.Id);

            entity.ShouldNotBeNull();
            entity.Description.ShouldBe(command.Description);
        }

        [Fact]
        public void Handle_GivenInvalidId_ThrowsException()
        {
            var command = new UpdateProductCommand
            {
                Id = 99,
                Description = "Null Product",
            };

            var handler = new UpdateProductCommand.UpdateProductCommandHandler(Context);

            Should.ThrowAsync<NotFoundException>(() =>
                handler.Handle(command, CancellationToken.None));
        }
    }
}
