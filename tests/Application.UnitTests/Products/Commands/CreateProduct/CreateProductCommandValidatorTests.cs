﻿using onlineshop.Application.Products.Commands.CreateProduct;
using onlineshop.Application.UnitTests.Common;
using onlineshop.Domain.Entities;
using onlineshop.Domain.Enums;
using onlineshop.Domain.ValueObjects;
using Shouldly;
using Xunit;

namespace onlineshop.Application.UnitTests.Products.Commands.CreateProduct
{
    public class UpdateProductCommandValidatorTests : CommandTestBase
    {
        [Fact]
        public void IsValid_ShouldBeTrue_WhenProductPriceIsPositive()
        {
            var command = new CreateProductCommand
            {
                Color = (int)Color.Green,
                Size = (int)Size.M,
                PriceStr = "USD 30",
                Description = "Green Tshirt"
            };

            var validator = new CreateProductCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(true);
        }

        [Fact]
        public void IsValid_ShouldBeFalse_WhenProductPriceIsNotPositive()
        {
            var command = new CreateProductCommand
            {
                Color = 2,
                Size = 3,
                PriceStr = "USD -1",
                Description = "Tshirt"
            };

            var validator = new CreateProductCommandValidator(Context);

            var result = validator.Validate(command);

            result.IsValid.ShouldBe(false);
        }
    }
}
