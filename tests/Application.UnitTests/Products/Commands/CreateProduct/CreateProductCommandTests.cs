﻿using onlineshop.Application.Products.Commands.CreateProduct;
using onlineshop.Application.UnitTests.Common;
using onlineshop.Domain.Enums;
using onlineshop.Domain.ValueObjects;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace onlineshop.Application.UnitTests.Products.Commands.CreateProduct
{
    public class CreateProductCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_ShouldPersistProduct()
        {
            var command = new CreateProductCommand
            {
                Color = (int)Color.Gray,
                Size = (int)Size.XL,
                PriceStr = "USD 32",
                Description = "Gray Tshirt"
            };

            var handler = new CreateProductCommand.CreateProductCommandHandler(Context);

            var result = await handler.Handle(command, CancellationToken.None);

            var entity = Context.Products.Find(result);

            entity.ShouldNotBeNull();
            entity.Description.ShouldBe(command.Description);
        }
    }
}
