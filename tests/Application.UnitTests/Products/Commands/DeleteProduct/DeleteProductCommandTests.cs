﻿using onlineshop.Application.Common.Exceptions;
using onlineshop.Application.Products.Commands.DeleteProduct;
using onlineshop.Application.UnitTests.Common;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace onlineshop.Application.UnitTests.Products.Commands.DeleteProduct
{
    public class DeleteProductCommandTests : CommandTestBase
    {
        [Fact]
        public async Task Handle_GivenValidId_ShouldRemovePersistedProduct()
        {
            var command = new DeleteProductCommand
            {
                Id = 1
            };

            var handler = new DeleteProductCommand.DeleteProductCommandHandler(Context);

            await handler.Handle(command, CancellationToken.None);

            var entity = Context.Products.Find(command.Id);

            entity.ShouldBeNull();
        }

        [Fact]
        public void Handle_GivenInvalidId_ThrowsException()
        {
            var command = new DeleteProductCommand
            {
                Id = 99
            };

            var handler = new DeleteProductCommand.DeleteProductCommandHandler(Context);

            Should.ThrowAsync<NotFoundException>(() =>
                handler.Handle(command, CancellationToken.None));
        }
    }
}
