﻿using AutoMapper;
using onlineshop.Application.Products.Queries.GetProducts;
using onlineshop.Application.UnitTests.Common;
using onlineshop.Infrastructure.Persistence;
using Shouldly;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace onlineshop.Application.UnitTests.Products.Queries.GetProducts
{
    [Collection("QueryTests")]
    public class GetProductsQueryTests
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetProductsQueryTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task Handle_ReturnsCorrectVmAndProductCount()
        {
            var query = new GetProductsQuery();

            var handler = new GetProductsQuery.GetProductsQueryHandler(_context, _mapper);

            var result = await handler.Handle(query, CancellationToken.None);

            result.ShouldBeOfType<ProductsVm>();
            result.Lists.Count.ShouldBe(1);
        }
    }
}
