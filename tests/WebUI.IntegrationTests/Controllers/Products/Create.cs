﻿using onlineshop.Application.Products.Commands.CreateProduct;
using onlineshop.Domain.ValueObjects;
using Shouldly;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace onlineshop.WebUI.IntegrationTests.Controllers.Products
{
    public class Create : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Create(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenValidCreateProductCommand_ReturnsSuccessCode()
        {
            var client = await _factory.GetAuthenticatedClientAsync();

            var command = new CreateProductCommand
            {
                Color = 2,
                Size = 1,
                Description = "TShirt",
                PriceStr = "USD 21"               
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PostAsync($"/api/products", content);

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenInvalidCreateProductCommand_ReturnsBadRequest()
        {
            var client = await _factory.GetAuthenticatedClientAsync();

            var command = new CreateProductCommand
            {
                PriceStr = "price_not_positive -1"
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PostAsync($"/api/products", content);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
    }
}
