﻿using onlineshop.Application.Common.Interfaces;
using System;

namespace onlineshop.WebUI.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
