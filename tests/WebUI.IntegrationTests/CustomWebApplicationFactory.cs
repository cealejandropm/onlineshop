﻿using onlineshop.Application;
using onlineshop.Application.Common.Interfaces;
using onlineshop.Domain.Entities;
using onlineshop.Infrastructure.Persistence;
using IdentityModel.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using onlineshop.Domain.ValueObjects;
using onlineshop.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;

namespace onlineshop.WebUI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder
                .ConfigureServices(async services =>
                {
                    // Create a new service provider.
                    var serviceProvider = new ServiceCollection()
                        .AddEntityFrameworkInMemoryDatabase()
                        .BuildServiceProvider();

                    // Add a database context using an in-memory 
                    // database for testing.
                    services.AddDbContext<ApplicationDbContext>(options =>
                    {
                        options.UseInMemoryDatabase("InMemoryDbForTesting");
                        options.UseInternalServiceProvider(serviceProvider);
                    });

                    services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());

                    services.AddScoped<ICurrentUserService, TestCurrentUserService>();
                    services.AddScoped<IDateTime, TestDateTimeService>();
                    services.AddScoped<IIdentityService, TestIdentityService>();


                    var sp = services.BuildServiceProvider();

                    // Create a scope to obtain a reference to the database
                    using var scope = sp.CreateScope();
                    var scopedServices = scope.ServiceProvider;
                    var context = scopedServices.GetRequiredService<ApplicationDbContext>();
                    var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    // Ensure the database is created.
                    context.Database.EnsureCreated();

                    try
                    {                        
                        SeedSampleData(context);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"An error occurred seeding the database with sample data. Error: {ex.Message}.");
                    }
                })
                .UseEnvironment("Test");
        }

        public HttpClient GetAnonymousClient()
        {
            return CreateClient();
        }

        public async Task<HttpClient> GetAuthenticatedClientAsync()
        {
            return await GetAuthenticatedClientAsync("cealejandropm@gmail.com", "Cambia.123");
        }

        public async Task<HttpClient> GetAuthenticatedClientAsync(string userName, string password)
        {
            var client = CreateClient();

            var token = await GetAccessTokenAsync(client, userName, password);

            client.SetBearerToken(token);

            return client;
        }

        private async Task<string> GetAccessTokenAsync(HttpClient client, string userName, string password)
        {
            var disco = await client.GetDiscoveryDocumentAsync();

            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }

            var response = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "onlineshop.IntegrationTests",
                ClientSecret = "secret",

                Scope = "onlineshop.WebUIAPI openid profile",
                UserName = userName,
                Password = password
            });

            if (response.IsError)
            {
                throw new Exception(response.Error);
            }

            return response.AccessToken;
        }

        public static void SeedSampleData(ApplicationDbContext context)
        {
            context.Products.AddRange(
                new Product { Id = 1, Color = Domain.Enums.Color.Blue, Size = Domain.Enums.Size.XL, Price=Price.For("USD 22"), Description="Blue Tshirt" },
                new Product { Id = 2, Color = Domain.Enums.Color.Brown, Size = Domain.Enums.Size.XL, Price = Price.For("USD 29"), Description = "Brown Boots" },
                new Product { Id = 3, Color = Domain.Enums.Color.Green, Size = Domain.Enums.Size.S, Price = Price.For("USD 20"), Description = "Green Tshirt" },
                new Product { Id = 4, Color = Domain.Enums.Color.Orange, Size = Domain.Enums.Size.XS, Price = Price.For("USD 26"), Description = "Orange Tshirt" },
                new Product { Id = 5, Color = Domain.Enums.Color.Purple, Size = Domain.Enums.Size.XL, Price = Price.For("USD 27"), Description = "Purple Tshirt" }
            );

            context.SaveChanges();
        }

        public async Task CreateUsersAndRoles(IServiceScope serviceScope)
        {
            var userManager = serviceScope.ServiceProvider.GetService<IIdentityService>();

            await userManager.CreateUserAsync("cealejandropm@gmail.com", "Cambia.123");
        }
    }
}
