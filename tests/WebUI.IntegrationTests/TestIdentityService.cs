﻿using Microsoft.AspNetCore.Identity;
using onlineshop.Application;
using onlineshop.Application.Common.Models;
using onlineshop.Infrastructure.Identity;
using System;
using System.Threading.Tasks;

namespace onlineshop.WebUI.IntegrationTests
{
    public class TestIdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public TestIdentityService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public Task<string> GetUserNameAsync(string userId)
        {
            return Task.FromResult("cealejandropm@gmail.com");
        }

        public async Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
        {
            var user = new ApplicationUser
            {
                UserName = userName,
                Email = userName,
            };

            var result = await _userManager.CreateAsync(user, password);

            return (result.ToApplicationResult(), user.Id);
        }

        public Task<Result> DeleteUserAsync(string userId)
        {
            throw new NotImplementedException();
        }
    }
}
