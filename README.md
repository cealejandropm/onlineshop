## Technolog�as
* .NET Core 3
* ASP .NET Core 3
* Entity Framework Core 3
* Angular 8

## Comenzando

1. Instalar [.NET Core SDK](https://dotnet.microsoft.com/download)
2. Navegar a `src/WebUI` y ejecutar `dotnet run` para ejecutar el proyecto

## Visi�n General

### Domain

Contiene las entidades, enums, exepciones, interfaces, tipos y l�gica spec�fica a la capa de dominio.

### Application

Esta capa contiene toda la l�gica de la aplicaci�n. Depende de la capa de dominio, pero no depende de ninguna otra capa o proyecto. Esta capa define las interfaces que implementan las capas externas. Por ejemplo, si la aplicaci�n necesita acceder a un servicio de notificaci�n, se agregar� una nueva interfaz a la aplicaci�n y se crear� una implementaci�n dentro de la infraestructura.

### Infrastructure

Esta capa contiene clases para acceder a recursos externos como sistemas de archivos, servicios web, smtp, etc. Estas clases deben basarse en interfaces definidas dentro de la capa de aplicaci�n.

### WebUI

Esta capa es una aplicaci�n de p�gina �nica basada en Angular 8 y ASP.NET Core 3. Esta capa depende de las capas de Aplicaci�n e Infraestructura, sin embargo, la dependencia de Infraestructura es solo para soportar la inyecci�n de dependencia. Por lo tanto, solo * Startup.cs * debe hacer referencia a Infraestructura.

